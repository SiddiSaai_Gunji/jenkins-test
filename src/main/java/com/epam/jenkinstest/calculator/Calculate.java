package com.epam.jenkinstest.calculator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Calculate {

    int add(int a, int b){
        log.info("Called add");
        return 10;
    }

}
