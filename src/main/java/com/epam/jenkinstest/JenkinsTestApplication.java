package com.epam.jenkinstest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class JenkinsTestApplication {

    public static void main(String[] args) {
        log.info("Started building , by : Siddhu");
        SpringApplication.run(JenkinsTestApplication.class, args);
    }

}
